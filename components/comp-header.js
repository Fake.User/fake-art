const templateCompHeader = document.createElement("template");
templateCompHeader.innerHTML = `
    <style>
        a{
            color: #fff;
            text-decoration: none;
        }
        .header{
            filter: drop-shadow(0 0 2vh #000);
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 9vh;
            background-color: #000e;
            display: grid;
            grid-template-columns: 0 auto 1fr auto auto auto 0;
            gap: 2.5vh;
            z-index: 9;
        }
            .logo{
                padding: 2vh 0;
            }
            .icon{
                padding: 3vh 0;
            }
            #contact{
                padding: 2vh 0;
            }


    </style>
    <div class="header shadow">
        <div><!--Spacer--></div>
        <comp-route route="/" class="logo">
            <comp-icon img="/assets/svg-lettermark.svg" alt="home" height="5vh"></comp-icon>
        </comp-route>
        <div><!--Spacer--></div>
        <comp-route class="icon" route="/open-source">
            <comp-icon img="/assets/svg-share.svg" alt="open-source" height="3vh"></comp-icon>
        </comp-route>
        <comp-route class="icon" route="/client-work">
            <comp-icon img="/assets/svg-briefcase.svg" alt="client-work" height="3vh"></comp-icon>
        </comp-route>
        <comp-button id="contact">
            <div>CONTACT</div>
        </comp-button>
    </div>
`;

class CompHeader extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompHeader.content.cloneNode(true));
    };
    connectedCallback(){
        this.shadowRoot.getElementById("contact").addEventListener("click", openContact => {
            this.dispatchEvent(new CustomEvent("open-contact", {bubbles: true, composed: true}));
        });
    };
};

window.customElements.define("comp-header", CompHeader);
