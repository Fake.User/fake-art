const templateCompLanding = document.createElement("template");
templateCompLanding.innerHTML = `
    <style>
        #landing{
            width: 100%;
            height: 100vh;
            mask: linear-gradient(#fff, #fff);
            -webkit-mask: linear-gradient(#fff, #fff);
        }
            #landing-video-wrapper{
                width: 100%;
                height: 91vh;
                position: fixed;
                top: 9vh;
                z-index:-10;
                will-change: transform; /* Helps eliminate jank but cuases weird sizing jump */
            }
                #landing-video{
                    width: 100%;
                    height: 100%;
                    object-fit: cover;
                }
            #text-wrapper{
                height: 100svh;
                mix-blend-mode: difference;
                font-kerning: none;
                display: flex;
                flex-flow: column;
                align-items: center;
                justify-content: center;

            }
                #spacer{
                    height: 9vh;
                }
                #text{
                    position: relative;
                    text-align: center;
                    font-size: min(20vh, 10vw);
                    line-height: min(18vh, 9vw);
                    letter-spacing: min(1.6vh, 0.8vw);
                    font-family: podium-sharp;
                    font-kerning: none;
                    z-index: 10;
                }
    </style>
    <div id="landing">
        <div id="landing-video-wrapper"><!--fix for safari video position: fixed; bug-->
            <video id="landing-video" type="video/mp4" src="" poster="" autoplay muted loop playsinline></video>
        </div>
        <div id="text-wrapper">
            <div id="spacer"></div>
            <div id="text"></div>
        </div>
    </div>
`;

class CompLanding extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompLanding.content.cloneNode(true));

        /* Style */
        this.shadowRoot.getElementById('landing-video').poster = this.getAttribute("poster");
        this.shadowRoot.getElementById('landing-video').src = this.getAttribute("vid");
        this.shadowRoot.getElementById('text').innerHTML = this.getAttribute("text");
    };
};

window.customElements.define("comp-landing", CompLanding);
