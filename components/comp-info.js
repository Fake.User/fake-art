const templateCompInfo = document.createElement("template");
templateCompInfo.innerHTML = `
    <style>
        a{
            color: #fff;
            text-decoration: none;
        }
        .info{
            position: relative;
            filter: drop-shadow(0 0 4vh #222);
            z-index: 5;
            width: 100%;
            height: 100%;
            background-color: #0c0c0c;
            background-image: url("/assets/webp-paper.webp");
            background-position: center;
            background-size: cover;
            padding: 8vh 0;
            text-align: center;
        }
            .info-wrapper{
                display: grid;
                justify-content: center;
                grid-template-columns: 1fr minmax(0%, 86vh) 1fr;
            }
                .text{
                    padding: 0 2vh;
                    grid-area: 1 / 2;
                }
                .button{
                    display: flex;
                    justify-content: center;
                    grid-area: 2 / 2;
                }

    </style>

    <div class="info">
        <div class="info-wrapper">
                <div class="text">
                    <slot name="text"></slot>
                </div>
                <div class="button">
                    <slot name="button"></slot>
                </div>
            </div>
        </div>
    </div>

`;

class CompInfo extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompInfo.content.cloneNode(true));
    };
};

window.customElements.define("comp-info", CompInfo);
