const templateCompIcon = document.createElement("template");
templateCompIcon.innerHTML = `
<style>
    #icon{
        width: auto;
        transition-duration: 1s;
        cursor: pointer;
        display: grid;
        align-self: center;

    }
    @media (hover){
        #icon:hover{
            filter: brightness(0) saturate(100%) invert(39%) sepia(33%) saturate(5358%) hue-rotate(2deg) brightness(103%) contrast(107%);
            transition-duration: 0s;
        }
    }
</style>
<img id="icon" src="" alt="">
`;

class CompIcon extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompIcon.content.cloneNode(true));

        /* Style */
        this.shadowRoot.getElementById('icon').src = this.getAttribute("img");
        this.shadowRoot.getElementById('icon').alt = this.getAttribute("alt");
        this.shadowRoot.getElementById('icon').style.height = this.getAttribute("height");
    };
};

window.customElements.define("comp-icon", CompIcon);
