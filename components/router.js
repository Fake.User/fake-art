const routes = {
    404: "/pages/404.html",
    "/": "projects/home.html",
    "/open-source": "/projects/open-source/open-source.html",
    "/open-source/fake-data": "/projects/open-source/fake-data/fake-data.html",
    "/client-work": "/projects/client-work/client-work.html",
    "/client-work/dood-dow": "/projects/client-work/dood-dow/dood-dow.html",
    "/client-work/frvr-mute": "/projects/client-work/frvr-mute/frvr-mute.html",
    "/client-work/holo-bh": "/projects/client-work/holo-bh/holo-bh.html",
    "/client-work/holo-mon": "/projects/client-work/holo-mon/holo-mon.html",
    "/client-work/jack-owl": "/projects/client-work/jack-owl/jack-owl.html",
    "/client-work/jack-tad": "/projects/client-work/jack-tad/jack-tad.html",
    "/client-work/lmbr-ntmr": "/projects/client-work/lmbr-ntmr/lmbr-ntmr.html",
    "/client-work/sdy-wuha": "/projects/client-work/sdy-wuha/sdy-wuha.html",
    "/client-work/wav-taor": "/projects/client-work/wav-taor/wav-taor.html",
};

const handleLocation = async () => {
    const path = window.location.pathname;
    const route = routes[path] || routes[404];
    const html = await fetch(route).then((data) => data.text());
    document.getElementById("fake-site").innerHTML = html;
    window.scrollTo(0, -1);
};

window.addEventListener("route", route => {
    let url = route.detail.url
    window.history.pushState({}, "", url);
    handleLocation();
});

window.onpopstate = handleLocation;
handleLocation();
