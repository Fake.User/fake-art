const templateCompProject = document.createElement("template");
templateCompProject.innerHTML = `
    <style>
    #project{
        width: 100%;
        aspect-ratio: 1 / 1;
        filter: drop-shadow(0 0 1vh #000);
        border-radius: 2.5vh;
        background: #0008;
        background-size: cover;
        display: block;
    }
        #project-text-wrapper{
            width: 100%;
            height: 100%;
            transition-duration: 1s;
            position: relative;

        }
        @media (hover){
            #project-text-wrapper{
                filter: opacity(0%);
            }
        }
        #project-text-wrapper:hover{
            filter: opacity(100%);
            transition-duration: 0s;
        }
            #project-text{
                width: 100%;
                padding: 4vh 0 3vh 0;
                background: #000a;
                display: grid;
                letter-spacing: 50%;
                text-align: center;
                line-height: calc(0.5vh + 1.5vw);
                position: absolute;
                bottom: 0;
                left: 0;
                border-radius: 0 0 2vh 2vh;
            }
                #name{
                    font-size: 4vh;
                    color: #fff;
                }
                #button{
                    justify-self: center;
                    padding-top: 2vh;
                }
    </style>
    <div id="project">
        <div id="project-text-wrapper">
            <div id="project-text">
                <span id="name"></span>
                <comp-button id="button"></comp-button>
            </div>
        </div>
    </div>
`;

class CompProject extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompProject.content.cloneNode(true)
        );

        /* Style */
        this.shadowRoot.getElementById("project").style.backgroundImage = "url('" + this.getAttribute("img") + "')";
        this.shadowRoot.getElementById("name").innerHTML = this.getAttribute("name");
        this.shadowRoot.getElementById("button").innerHTML = this.getAttribute("button");
    }
};

window.customElements.define("comp-project", CompProject);
