const templateCompRoute = document.createElement("template");
templateCompRoute.innerHTML = `
    <style>
        .route{
            cursor: pointer;
        }
    </style>
    <div class="route">
        <slot></slot>
    </div>
`;

class CompRoute extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompRoute.content.cloneNode(true));
    };
    connectedCallback(){
        this.shadowRoot.addEventListener("click", route => {
            let url = this.getAttribute("route");
            this.dispatchEvent(new CustomEvent("route", {detail: {url: url}, bubbles: true, composed: true}))
        });
    };
};

window.customElements.define("comp-route", CompRoute);
