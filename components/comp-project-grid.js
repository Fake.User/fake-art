const templateCompProjectGrid = document.createElement("template");
templateCompProjectGrid.innerHTML = `
    <style>
    a{
        text-decoration: none;
    }
    .projects{
        box-sizing: border-box;
        width: 100%;
        background-color: #111;
        padding: 4vh;
        display: grid;
        grid-template-columns: auto min(100%, 180vh) auto;
        z-index: 4;
        position: relative;
    }
        .project-grid{
            grid-area: 1 / 2;
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(min(45vh, 100%), 1fr));
            gap: 4vh;
        }
        #button{
            grid-area: 2 / 2;
            justify-self: center;
        }
    </style>
    <div class="projects">
        <div class="project-grid">
            <slot name="projects"></slot>
        </div>
        <div id="button">
            <slot name="button"></slot>
        </div>
    </div>
`;

class CompProjectGrid extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompProjectGrid.content.cloneNode(true));
        if(this.getAttribute("button") === "true"){
            this.shadowRoot.getElementById("button").style.paddingTop = "4vh";
        };
    };
};

window.customElements.define("comp-project-grid", CompProjectGrid);
