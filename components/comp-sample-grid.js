const templateCompSampleGrid = document.createElement("template");
templateCompSampleGrid.innerHTML = `
    <style>
        #sample-wrapper{
            width: 100%;
            display: grid;
            grid-template-columns: auto min(100%, 150vh) auto;
            background-color: #080808;
            color: #666;
        }
            #samples{
                grid-area: 1 / 2;
                display: grid;
                padding: 4vh 2vh;
            }
                .sample{
                    height: min-content;
                    height: 8vh;
                    display: grid;
                    grid-template-columns: 8vh 8vh 8vh auto 8vh 8vh 3vh 3vh;
                    justify-items: center;
                    align-items: center;
                    padding: 0 2vh;
                }
                .divider{
                    width: 100%;
                    background-color: #181818;

                    height: 0.4vh;
                    border-radius: 0.2vh;
                }
    </style>
        <div id="sample-wrapper">
            <div id="samples"></div>
        </div>
`;

class CompSampleGrid extends HTMLElement{
    constructor(){
        super();
    };

    connectedCallback(){
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.appendChild(templateCompSampleGrid.content.cloneNode(true));

        /*----- CREATE PLAYLIST -----*/
        fetch("/projects/open-source/fake-data/test-res.json")
        .then((res) => res.json())
        .then((data) => {
            let output = `<div class="divider""></div>`;
            data.forEach(function (sample) {
                output += `
                <div class="sample"">
                    <div>${sample.primary}</div>
                    <div>${sample.secondary}</div>
                    <div>${sample.name}</div>
                    <div>${sample.wavform}</div>
                    <div>${sample.key}</div>
                    <div>${sample.bpm}bpm</div>
                    <img class="" src="/assets/download.svg">
                    <img class="" src="/assets/play.svg">
                </div>
                <div class="divider"></div>`
            });
            shadowRoot.getElementById("samples").innerHTML = output;
        });
    };
};
window.customElements.define("comp-sample-grid", CompSampleGrid);
