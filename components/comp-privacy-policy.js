const templateCompPrivacyPolicy = document.createElement("template");
templateCompPrivacyPolicy.innerHTML = `
    <style>
        a{
            text-decoration: none;
        }
        #privacy{
            background-color: #000e;
            position: fixed;
            z-index: 11;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            display: none;
            grid-template-columns: auto min(100%, 60vh) auto;
            align-items: center;
        }
            #policy{
                width: 100%;
                grid-area: 1 / 2;
                display: grid;
                grid-gap: 2vh;
                text-align: center;
            }
                #tools{
                    display: flex;
                    flex-flow: row wrap;
                    grid-gap: 2vh;
                    justify-content: center;
                }
                #close{
                    justify-self: center;
                }
    </style>
    <div id="privacy">
        <div id="policy">
            <div>
                fakeart.online has a very simple privacy policy: Nothing about you is logged or tracked, & there are no cookies or analytics.
            </div>
            <div>
                If you have reached out via the contact form & want any conversations deleted, I will happily delete them upon request.
            </div>
            <div>
                Your privacy is yours to keep, which should really be the default. Unfortunately, it really isn't. Here are some tools I like to help take it back:
            </div>
            <div id="tools">
                <div><!--spacer--></div>
                <a href="https://proton.me/" target="_blank">
                    <comp-button>PROTON&nbspMAIL</comp-button>
                </a>
                <a href="https://www.signal.org/" target="_blank">
                    <comp-button>SIGNAL</comp-button>
                </a>
                <a href="https://duckduckgo.com/spread" target="_blank">
                    <comp-button>DUCK&nbspDUCK&nbspGO</comp-button>
                </a>
            </div>
            <comp-icon id="close" img="/assets/svg-x.svg" alt="close" height="3vh"></comp-icon>
        </div>
    </div>
`;

class CompPrivacyPolicy extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompPrivacyPolicy.content.cloneNode(true));
    };
    connectedCallback(){
        this.shadowRoot.getElementById("close").addEventListener("click", closePrivacyPolicy => {
            this.shadowRoot.getElementById("privacy").style.display = "none";
        });
        window.addEventListener("open-privacy-policy", openPrivacyPolicy => {
            this.shadowRoot.getElementById("privacy").style.display = "grid";
        });
    };
};

window.customElements.define("comp-privacy-policy", CompPrivacyPolicy);
