const templateCompSearch = document.createElement("template");
templateCompSearch.innerHTML = `
    <style>
        #landing{
            width: 100%;
            position: fixed;
            top: 0;
            background-color: #000;
            background-image: url("/assets/paper.png");
            background-size: cover;
        }
            #form-wrapper{
                padding: 13vh 0 4vh 0;
                font-kerning: none;
                display: grid;
                grid-template-columns: auto min(100%, 90vh) auto;
                grid-template-areas: "left center right";
                align-items: center;
                justify-content: center;
                color: #f00;
            }
                #form{
                    z-index: 20;
                    grid-area: center;
                    display: grid;
                    grid-template-rows: repeat(4, min-content);
                    grid-gap: 2vh;
                    filter: drop-shadow(0 0 2vh #000);
                }
                    .search-wrapper{
                        display: grid;
                        grid-gap: 1vh;
                        grid-template-columns: 1fr min-content min-content min-content;
                        align-items: center;
                        justify-self: center;
                        width: min(100%, 45vh);
                        height: 5vh;
                        border: 0.3vh solid #444;
                        border-radius: 2.5vh;
                        transition-duration: 0.5s;
                        padding: 0 2vh;
                        background-color: #2224;
                    }
                        .search-wrapper:has(#search:focus){
                            border-color: #f00;
                            transition-duration: 0s;
                        }
                    #search{
                        background-color: #0000;
                        border: none;
                        font-size: 2vh;
                        font-family: cq-mono;
                        letter-spacing: 0.09em;
                        caret-color: #f00;
                        color: #f00;
                    }
                        #search::placeholder{
                            color: #f00;
                            opacity: 1;
                        }
                        #search:focus{
                            outline: 0;
                        }
                            .search-icon{
                                heigth: 3vh;
                                width: auto;
                                filter: saturate(0%) brightness(126%);
                                transition-duration: 0.5s;
                                cursor: pointer;
                            }

                        @media (hover){
                            .search-wrapper:hover{
                                border-color: #f00;
                                transition: filter 0s;
                            }
                            #search::selection{
                                color: #000;
                                background: #f00;
                            }
                            .search-icon:hover{
                                filter: saturate(100%) brightness(100%);
                                transition-duration: 0s;
                            }
                        }
                .radio{
                    display: none;
                }
                .radio:checked + .tag{
                    background-color: #f004;
                    border-color: #f00;
                    transition-duration: 0s;
                    color: #f00;
                }
                .tags{
                    display: flex;
                    flex-wrap: wrap;
                    align-self: center;
                    justify-self: center;
                    justify-content: center;
                    align-items: center;
                    gap: 1vh;
                    width: 100%;
                }
                    .tag{
                        background-color: #2224;
                        padding: 0 1vh;
                        border: 0.25vh solid #444;
                        color: #444;
                        border-radius: 0.4vh;
                        line-height: 2vh;
                        font-size: 1.6vh;
                        height: min-content;
                        transition-duration: 0.5s;
                        cursor: pointer;

                    }
                        .tag:hover{
                            border-color: #f00;
                            color: #f00;
                            transition: border-color 0s;
                        }
                    .category{
                        border: 0.35vh solid #444;
                        border-radius: 100%;
                        transition-duration: 0.5s;
                        width: min(16vh, 10vw);
                        height: min(16vh, 10vw);
                        aspect-ratio: 1 / 1;
                        background-color: #2224;
                        cursor: pointer;
                    }
                        .category:hover{
                            border-color: #f00;
                            transition-duration: 0s;
                        }
                        .category-image{
                            width: 100%;
                            height: 100%;
                        }
                        .radio:checked + .category{
                            border-color: #f00;
                            transition-duration: 0s;
                        }
        #search-spacer{
            height: 49vh;
        }
        #sort-filter{
            width: 100%;
            background-color: #141414;
            padding: 2vh 0;
            filter: drop-shadow(0 0 4vh #000);
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 4vh;
            color: #666;
            font-size: 2vh;
        }
    </style>
    <div id="landing">
        <div id="form-wrapper">
            <form name="contact" method="POST" id="form">
                <div class="tags" id="cat" style="justify-content: space-around;">
                    <div><!--spacer--></div>
                    <div><!--spacer--></div>
                    <input type="radio" class="radio" id="Samples" name="catagory">
                    <label for="Samples" class="category">
                        <img class="category-image" src="/projects/open-source/fake-data/1.webp">
                    </label>
                    <input type="radio" class="radio" id="Textures" name="catagory">
                    <label for="Textures" class="category">
                        <img class="category-image" src="/projects/open-source/fake-data/2.webp">
                    </label>
                    <input type="radio" class="radio" id="Materials" name="catagory">
                    <label for="Materials" class="category">
                        <img class="category-image" src="/projects/open-source/fake-data/3.webp">
                    </label>
                    <div><!--spacer--></div>
                    <div><!--spacer--></div>
                </div>
                <div class="search-wrapper">
                    <input id="search" name="name" type="text" placeholder="SEARCH" required></input>
                    <img class="search-icon" id="clear-tags" src="/assets/x.svg">
                    <img class="search-icon" id="search-scale" data-scale="large" src="/assets/svg-minimize.svg">
                    <img class="search-icon" src="/assets/svg-search.svg">
                </div>
                <div id="primary" class="tags"></div>
                <div id="secondary" class="tags"></div>
            </form>
        </div>
        <div id="sort-filter">
            <div>Sort:</div>
            <div>A-Z</div>
            <div>Recent</div>
            <div>Downloads</div>
        </div>
    </div>
    <div id="search-spacer"></div>
`;

class CompSearch extends HTMLElement{
    constructor(){
        super();
    };

    connectedCallback(){
        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.appendChild(templateCompSearch.content.cloneNode(true));

        /* STATE */
        let stateCategory = "Samples";
        let statePrimary = "✶";
        let stateSecondary = "";
        let primaryTags = "";
        let secondaryTags = "";

        /* HANDLE CLEAR TAGS */
        shadowRoot.getElementById("clear-tags").addEventListener('click', event => {
            statePrimary = "✴";
            stateSecondary = "";
            pushState(dataJson);
        });

        /* HANDLE SEARCH SCALE */
        shadowRoot.getElementById("search-scale").addEventListener('click', event => {
            if(shadowRoot.getElementById("search-scale").getAttribute("data-scale") === "large"){
                shadowRoot.getElementById("search-scale").src = "/assets/svg-maximize.svg";
                shadowRoot.getElementById("cat").style.display = "none";
                shadowRoot.getElementById("primary").style.display = "none";
                shadowRoot.getElementById("secondary").style.display = "none";
                shadowRoot.getElementById("form").style.gridTemplateRows = "min-content";
                shadowRoot.getElementById("search-scale").setAttribute("data-scale", "small");
            }else{
                shadowRoot.getElementById("search-scale").src = "/assets/svg-minimize.svg";
                shadowRoot.getElementById("form").style.gridTemplateRows =  "repeat(4, min-content)";
                shadowRoot.getElementById("cat").style.display = "flex";
                shadowRoot.getElementById("primary").style.display = "flex";
                shadowRoot.getElementById("secondary").style.display = "flex";
                shadowRoot.getElementById("search-scale").setAttribute("data-scale", "large");
            }
            getHeight();
        });

        /* CHECK CATEGORY */
        function getCategory(data){
            for (var c = 0; c < data.length; c++){
                if(data[c].type === stateCategory){
                    return data[c]
                }
            }
            return data[0]
        };

        /* CHECK PRIMARY STATE */
        function getPrimary(data){
            primaryTags = ""; //reset tags
            data.types.forEach(function(tag){
                primaryTags += `
                    <input type="radio" class="radio" id="${tag.type}-primary" name="primary">
                    <label class="tag primary-tag" for="${tag.type}-primary">${tag.type}</label>
                `;
            });
            for (var p = 0; p < data.types.length; p++){
                if(data.types[p].type === statePrimary){
                    return data.types[p]
                }
            }
            return data.types[0]
        };

        /* CHECK SECONDARY STATE */
        function getSecondary(data){
            secondaryTags = ""; //reset tags
            data.types.forEach(function(tag){
                secondaryTags += `
                    <input type="radio" class="radio" id="${tag}-secondary" name="secondary">
                    <label class="tag secondary-tag" for="${tag}-secondary">${tag}</label>
                `
            });
            for (var s = 0; s < data.length; s++){
                if(data[s] == stateSecondary){
                    console.log(stateSecondary)
                }
            }
        };

        /* HANDLE CATEGORY */
        shadowRoot.querySelectorAll(".radio").forEach(tag => {
            tag.addEventListener('click', event => {
                console.log(tag.id);
                stateCategory = tag.id;
                statePrimary = "✴";
                stateSecondary = "";
                pushState(dataJson);
            })
        });

        /* HANDEL HEIGHT */
        function getHeight(){
            let spacerHeight = (shadowRoot.getElementById("landing").clientHeight / window.innerHeight) * 100;
            shadowRoot.getElementById("search-spacer").style.height = `${spacerHeight}vh`;
        };

        /* PUSH STATE */
        function pushState(data){
            let category = getCategory(data);
            let primary = getPrimary(category);
            let secondary = getSecondary(primary);
            if(statePrimary != "✶"){
                shadowRoot.getElementById("search").setAttribute("placeholder", `${stateCategory}: ${statePrimary}・${stateSecondary}`)
            }else{
                shadowRoot.getElementById("search").setAttribute("placeholder", `${stateCategory}: ${statePrimary}`)
            };

            shadowRoot.getElementById(stateCategory).checked = true;
            shadowRoot.getElementById("primary").innerHTML = primaryTags;
            shadowRoot.getElementById(statePrimary + "-primary").checked = true;
            shadowRoot.getElementById("secondary").innerHTML = secondaryTags;
            if(statePrimary != "✶"){
                shadowRoot.getElementById(stateSecondary + "-secondary").checked = true;
            };

            shadowRoot.querySelectorAll(".primary-tag").forEach(tag => {
                tag.addEventListener('click', event => {
                    statePrimary = tag.innerHTML;
                    stateSecondary = "✶";
                    pushState(dataJson);
                })
            });

            shadowRoot.querySelectorAll(".secondary-tag").forEach(tag => {
                tag.addEventListener('click', event => {
                    stateSecondary = tag.innerHTML;
                    pushState(dataJson);
                })
            });

            getHeight();
            scrollTo(0, 0);
        };

        /* GET JSON */
        let dataJson = "";
        async function getJson() {
            const response = await fetch("/projects/open-source/fake-data/fake-data.json")
            dataJson = await response.json();
            console.log(dataJson);
            pushState(dataJson);
        };
        getJson();
    };
};

window.customElements.define("comp-search", CompSearch);
