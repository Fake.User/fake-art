const templateCompFeatureGrid = document.createElement("template");
templateCompFeatureGrid.innerHTML = `
    <style>
        .features{
            box-sizing: border-box;
            background-color: #1a1a1a;
            padding: 4vh min(4vh, 2vw);
            display: grid;
            grid-template-columns: auto min(100%, 180vh) auto;
        }
            .features-grid{
                grid-area: 1 / 2;
                display: grid;
                gap: 4vh;
                justify-content: center;
            }
            #button{
                grid-area: 2 / 2;
                justify-self: center;
                padding-top: 4vh;
            }
    </style>
    <div class="features">
        <div class="features-grid">
            <slot name="feature"></slot>
        </div>
        <div id="button">
            <slot name="button"></slot>
        </div>
    </div>
`;

class CompFeatureGrid extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompFeatureGrid.content.cloneNode(true));
    };
};

window.customElements.define("comp-feature-grid", CompFeatureGrid);
