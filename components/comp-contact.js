const templateCompContact = document.createElement("template");
templateCompContact.innerHTML = `
    <style>
        #contact{
            background-color: #000e;
            position: fixed;
            z-index: 11;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            display: none;
            grid-template-columns: auto min(100%, 50vh) auto;
            align-items: center;
        }
            #contact-form{
                width: 100%;
                grid-area: 1 / 2;
                display: grid;
                grid-gap: 2vh;
                text-align: center;
            }
                input, textarea{
                    display: block;
                    box-sizing: border-box;
                    background-color: #0000;
                    width: min(100%, 180vh);
                    border: 0.3vh solid #fff;
                    border-radius: 2.5vh;
                    min-height: 5vh;
                    font-size: 2vh;
                    font-family: cq-mono;
                    letter-spacing: 0.09em;
                    line-height: 1.5em;
                    color: #fff;
                    padding-left: 2vh;
                    transition-duration: 1s;
                    caret-color: #f60;
                }
                    @media (hover){
                        input:hover{
                            border-color: #f60;
                            transition: filter 0s;
                        }
                        input:hover::placeholder{
                            color: #f60;
                            opacity: 1;
                            transition: filter 0s;
                        }
                        input::selection{
                          color: #000;
                          background: #f60;
                        }
                        textarea:hover{
                            border-color: #f60;
                            transition: filter 0s;
                        }
                        textarea:hover::placeholder{
                            color: #f60;
                            opacity: 1;
                            transition: filter 0s;
                        }
                        textarea::selection{
                          color: #000;
                          background: #f60;
                        }
                    }
                    input::placeholder{
                        text-align: center;
                    }
                    textarea::placeholder{
                        text-align: center;
                    }
                    input:focus{
                        border-color: #f60;
                        transition: filter 0s;
                        outline:0;
                    }
                    textarea:focus{
                        border-color: #f60;
                        transition: filter 0s;
                        outline:0;
                    }
                textarea{
                    min-height: 18vh;
                    padding: 2vh;
                    resize: none;
                }
                #submit{
                    border: none;
                    background-color: #0000;
                    font-size: 2vh;
                    justify-self: center;
                    font-family: cq-mono;
                    letter-spacing: 0.09em;
                    line-height: 1.5em;
                    color: #fff;
                }
                #close{
                    justify-self: center;
                }
        #submited{
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100svh;
            display: none;
            text-align: center;
            align-items: center;
            font-size: 4vh;
            line-height: 4vh;
        }
    </style>
    <div id="contact">
        <form name="contact" method="POST" data-netlify="true" id="contact-form">
            <input name="name" type="text" placeholder="NAME" required></input>
            <input name="email" type="email" placeholder="EMAIL" required></input>
            <textarea name="message" type="text" placeholder="MESSAGE" required></textarea>
            <button type="submit" id="submit">
                <comp-button>
                    SEND
                </comp-button>
            </button>
            <comp-icon id="close" img="/assets/svg-x.svg" alt="close" height="3vh"></comp-icon>
            <div style="display: none;"><input type="text" name="honeypot"></div>
            <input type="hidden" name="form-name" value="contact"/>
        </form>
        <div id="submited">
            <div>THANKS FOR REACHING OUT<br>CHEERS MATE</div>
        </div>
    </div>
`;

class CompContact extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompContact.content.cloneNode(true));
    };
    connectedCallback(){
        const handleSubmit = (event) => {
            event.preventDefault();
            const myForm = event.target;
            const formData = new FormData(myForm);
            fetch("/", {
                method: "POST",
                headers: { "Content-Type": "application/x-www-form-urlencoded" },
                body: new URLSearchParams(formData).toString(),
            })
            .then(this.shadowRoot.getElementById("contact-form").style.display = "none")
            .then(this.shadowRoot.getElementById("submited").style.display = "grid")
            .then(setTimeout(() => this.shadowRoot.getElementById("contact").style.display = "none", 2000))
            .catch((error) => alert(error));
        };

        this.shadowRoot.getElementById("contact-form").addEventListener("submit", handleSubmit);

        this.shadowRoot.getElementById("close").addEventListener("click", closeContact => {
            this.shadowRoot.getElementById("contact").style.display = "none";
        });
        window.addEventListener("open-contact", openContact => {
            this.shadowRoot.getElementById("contact-form").style.display = "grid";
            this.shadowRoot.getElementById("submited").style.display = "none";
            this.shadowRoot.getElementById("contact").style.display = "grid";
        });
    };
};

window.customElements.define("comp-contact", CompContact);
