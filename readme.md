[<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/fakeart%20-%20frontend.webp" alt="fakeart" width="100%">](https://fakeart.online)

# FAKEART
FAKE ART is my somewhat outdated art porfolio. (I mean, does any artist actually have their portfolio up to date)?
It's a single page app built from scratch with web compoments, and implements a pretty cute [clientside router](https://gitlab.com/Fake.User/fake-art/-/blob/main/components/router.js?ref_type=heads).

You can find the live website at [fakeart.online](https://fakeart.online)
