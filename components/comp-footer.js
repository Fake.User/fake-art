const templateCompFooter = document.createElement("template");
templateCompFooter.innerHTML = `
    <style>
        a{
            color: #fff;
            text-decoration: none;
        }
        .footer{
            min-height: 36vh;
            mask: linear-gradient(#fff, #fff);
            -webkit-mask: linear-gradient(#fff, #fff);
        }
        .footer-wrapper{
            background-color: #000;
            width: 100vw;
            display: grid;
            grid-template-columns: 1fr minmax(0%, 60vh) 1fr;
            padding: 4vh 0;
            position: fixed;
            z-index: -1;
            bottom: 0px;
        }
            .footer-info{
                text-align: center;
                grid-area: 1 / 2;
            }
            .footer-contact{
                grid-area: 2 / 2;
                justify-self: center;
                padding-top: 2vh;
            }
            .footer-links{
                grid-area: 3 / 2;
                display: grid;
                padding-top: 1vh;
                grid-template-columns: 1fr auto auto auto auto auto 1fr;
                gap: 2.5vh;
            }
                .logo{
                    padding: 2vh 0;
                }
                .icon{
                    padding: 2.5vh 0;
                }
            #privacy-policy{
                grid-area: 4 / 2;
                text-align: center;
                transition-duration: 1s;
                cursor: pointer;
            }
                @media (hover){
                    #privacy-policy:hover{
                        color: #f60;
                        transition-duration: 0s;
                    }
                }

    </style>
    <div class="footer">
        <div class="footer-wrapper">
            <div class="footer-info">
                Thanks for checking out fakeart.online. Want to connect? You can send me an email:
            </div>
            <comp-button class="footer-contact" type="email" email="fake@fakeart.online">
                fake@fakeart.online
            </comp-button>
            <div class="footer-links">
                <div><!--spacer--></div>
                <a class="icon" href="https://gitlab.com/Fake.User" target="_blank">
                    <comp-icon img="/assets/svg-gitlab.svg" alt="gitlab" height="3vh"></comp-icon>
                </a>
                <a class="icon"  href="https://discord.gg/QtbRGDAxSk" target="_blank">
                    <comp-icon img="/assets/svg-discord.svg" alt="discord" height="3vh"></comp-icon>
                </a>
                <comp-route route="/" class="logo">
                    <comp-icon img="/assets/svg-lettermark.svg" alt="home" height="5vh"></comp-icon>
                </comp-route>
                <comp-route class="icon" route="/open-source">
                    <comp-icon img="/assets/svg-share.svg" alt="open-source" height="3vh"></comp-icon>
                </comp-route>
                <comp-route class="icon" route="/client-work">
                    <comp-icon img="/assets/svg-briefcase.svg" alt="client-work" height="3vh"></comp-icon>
                </comp-route>
                <div><!--spacer--></div>
            </div>
            <div id="privacy-policy">
                PRIVACY&nbspPOLICY
            </div>
        </div>
    </div>
`;

class CompFooter extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompFooter.content.cloneNode(true));
    };
    connectedCallback(){
        this.shadowRoot.getElementById("privacy-policy").addEventListener("click", openPrivacyPolicy => {
            this.dispatchEvent(new CustomEvent("open-privacy-policy", {bubbles: true, composed: true}));
        });
    };
};

window.customElements.define("comp-footer", CompFooter);
