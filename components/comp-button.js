const templateCompButton = document.createElement("template");
templateCompButton.innerHTML = `
    <style>
        #button{
            display: grid;
            color: #000;
            background-color: #fff;
            background: linear-gradient(305deg, #f60 35%, #f00 100%);
            filter: grayscale(100%) brightness(400%);
            border-radius: 2.5vh;
            height: 5vh;
            font-size: 2vh;
            width: min-content;
            padding: 0 1.2em 0 1.2em;
            align-items: center;
            cursor: pointer;
            transition: filter 1s;
        }
            @media (hover){
                #button:hover{
                    filter: grayscale(0%) brightness(100%);
                    transition: filter 0s;
                }
            }
    </style>
    <div id="button">
        <slot></slot>
    </div>
`;

class CompButton extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompButton.content.cloneNode(true));
    };
    connectedCallback(){
        if(this.getAttribute("type") === "email"){
            this.shadowRoot.getElementById("button").addEventListener("click", emailCopied => {
                let email = this.getAttribute("email")
                navigator.clipboard.writeText(email);
                this.shadowRoot.getElementById("button").innerHTML = "&nbsp&nbsp&nbspemail&nbsp&nbspcopied&nbsp&nbsp&nbsp";
                setTimeout(() => this.shadowRoot.getElementById("button").innerHTML = email, 2000);
            })
        }
    }
};

window.customElements.define("comp-button", CompButton);
