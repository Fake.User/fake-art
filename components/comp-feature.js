const templateCompFeature = document.createElement("template");
templateCompFeature.innerHTML = `
    <style>
        #feature{
            display: flex;
            flex-wrap: wrap;
            flex-direction: row;
            background-color: #080808;
            filter: drop-shadow(0 0 1vh #000);
            border-radius: 2.5vh;
            box-sizing: border-box;
            width: 100%;

            align-items: center;
        }
            #text{
                width: min(100%, 40vh);
                flex-grow: 1;
                padding: 4vh;
            }
                #caption{
                    text-align: center;
                    font-size: 5vh;
                    line-height: 6vh;
                    font-family: podium-sharp;
                    padding-bottom: 3vh;
                    font-kerning: none;
                }

            #content{
                width: min(100%, 60vh);
                height: 100%;
                aspect-ratio: 16 / 9;
                box-sizing: border-box;
                flex-grow: 1;
            }

    </style>
    <div id="feature">
        <div id="content"></div>
        <div id="text">
            <div id="caption">
                <slot name="caption"></slot>
            </div>
            <slot name="text"></slot>
        </div>
    </div>
`;

class CompFeature extends HTMLElement{
    constructor(){
        super();
        this.attachShadow({mode: "open"});
        this.shadowRoot.appendChild(templateCompFeature.content.cloneNode(true));
        /* Style */
        this.shadowRoot.getElementById("feature").style.flexDirection = this.getAttribute("flex-direction");
        if(this.getAttribute("type") === "video"){
            let video = this.getAttribute("video");
            let img = this.getAttribute("img");
            let content = `<video src="${video}" poster="${img}" style="width: 100%; height: 100%; border-radius: 2vh;" autoplay muted loop playsinline></video>`;
            this.shadowRoot.getElementById("content").innerHTML = content;
        }else{
            let content = `<img src="${this.getAttribute("img")}" style="width: 100%; height: 100%; border-radius: 2vh;">`;
            this.shadowRoot.getElementById("content").innerHTML = content;
        };
    };
};

window.customElements.define("comp-feature", CompFeature);
